#Picpicker 
##根据图书ISBN抓图书封面图片的小程序

###需要安装BeautifulSoup
[参考BS4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/index.zh.html#)

###源码清单
- isbn.txt ISBN样本文件
- picpicker.py 抓取程序
- picpicker_mt.py 抓取程序（多线程）
 
###截图
![输入图片说明](http://git.oschina.net/uploads/images/2017/0315/155749_d0a51f58_537766.jpeg "在这里输入图片标题")

# interlib 图片抓取
抓取 interlib 图书数据【isbn资源】 然后在京东爬取图片
暂时只是单线程方式，interlib有爬取规则1s钟只允许一次请求，太快反而取不到数据
对于大分类，interlib可能会无法响应，可能会导致对方系统崩溃